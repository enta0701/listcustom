package com.endo.entaku.listcustom;

import android.graphics.Bitmap;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by entaku on 15/05/05.
 */


public class CustomData {
    private Bitmap imageData;
    @Getter @Setter private String textData;
    @Getter @Setter private String textData2;

    public void setImagaData(Bitmap image) {
        imageData = image;
    }

    public Bitmap getImageData() {
        return imageData;
    }
//
//    public void setTextData(String text) {
//        textData = text;
//    }
//
//    public String getTextData() {
//        return textData;
//    }
//
//    public void setTextData2(String text) {
//        textData2 = text;
//    }
//
//    public String getTextData2() {
//        return textData2;
//    }

}
